<?php

	if(!isset($argv[1])) {
		echo 'Usage: BubbleSort.php <NUMBERS>' . PHP_EOL;
		exit;
	}

	$ns = array_slice($argv, 1);

	$originalCount = count($ns);

	foreach($ns as $k => $n) {
		if($n < 1 || $n > 99) {
			unset($ns[$k]);
		}
	}

	$ns = array_values($ns);

	$c = count($ns);
	$s = false;

	while($s == false) {
		$s = true;
		for($i = 1; $i < $c; $i++) {
			if($ns[$i] > $ns[$i - 1]) {
				$v = $ns[$i - 1];
				$ns[$i - 1] = $ns[$i];
				$ns[$i] = $v;
				$s = false;
			}
		}
	}

	$ns = array_reverse($ns);
	$count = count($ns);

	$res = [
		'originalCount' => $originalCount,
		'count' => $count,
		'countRemoved' => ($originalCount - $count),
		'numbersArray' => $ns,
		'numbersWithCommas' => implode(', ', $ns),
		'numbersWithSpaces' => implode(' ', $ns)
	];

	print_r($res);