###Simple PHP Bubble Sort
####Usage
```BubbleSort.php <NUMBERS>```

```<NUMBERS>``` may contain any amount of numbers however, numbers above 99 and/or below 1 are removed.
####Output
```$ php BubbleSort.php 1 6 3 8 4 93 4 7 106 2 6 904 8```

Will return...

```
Array
(
    [originalCount] => 13
    [count] => 11
    [countRemoved] => 2
    [numbersArray] => Array
        (
            [0] => 1
            [1] => 2
            [2] => 3
            [3] => 4
            [4] => 4
            [5] => 6
            [6] => 6
            [7] => 7
            [8] => 8
            [9] => 8
            [10] => 93
        )

    [numbersWithCommas] => 1, 2, 3, 4, 4, 6, 6, 7, 8, 8, 93
    [numbersWithSpaces] => 1 2 3 4 4 6 6 7 8 8 93
)
```
Where...

```originalCount``` is the amount of numbers passed.

```count``` is the final amount of numbers.

```countRemoved``` is the amount of numbers that were removed (```originalCount``` - ```count```).

```numbersArray``` is the sorted numbers in array form.

```numbersWithCommas``` is the sorted numbers as a string, separated by a commas (and spaces).

```numbersWithSpaces``` is the sorted numbers as a string, separated by spaces.